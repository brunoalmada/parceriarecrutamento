# README #

In this project, I applied some concepts of **AngularJS**, to learn the basic concepts behind AngularJS, and get the knowledge about it.

Also in this project, I applied the concept of **Generic repository pattern**. 
In the **"ParceriaRecrutamento.Dados"** project, there is a base class with all the **CRUD** operations, which is common to every entity in the context. And the entity classes (EstadosRepositorio, CandidatoRepositorio, GrauInstrucaoRepositorio) **inherits the base class**, so we have a fully functional repository specific to each entity.

### Who do I talk to? ###

* Bruno Almada
* * brunoha@gmail.com
* * [My LinkedIn profile](http://linkedin.com/in/brunoalmada)
* * [My Xing profile](https://www.xing.com/profile/Bruno_HeitzmannAlmada)
* * [about.me](https://about.me/brunoha)