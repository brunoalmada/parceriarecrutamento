﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ParceriaRecrutamento.Dados;

namespace ParceriaRecrutamento.Negocio
{
    public class CandidatoNegocio : IDisposable
    {
        private CandidatoRepositorio _repositorio;
        private EstadosRepositorio _estadosRepositorio;
        private GrauInstrucaoRepositorio _grauInstrucaoRepositorio;

        public CandidatoNegocio()
        {
            _repositorio = new CandidatoRepositorio();
            _estadosRepositorio = new EstadosRepositorio();
            _grauInstrucaoRepositorio = new GrauInstrucaoRepositorio();
        }

        public void Inserir(string json)
        {
            //var candidato = new JsonSerializer().Deserialize<Candidatos>(reader);
            var resultado = Newtonsoft.Json.JsonConvert.DeserializeObject<Candidatos>(json);
        }

        public string GetEstados()
        {
            return JsonConvert.SerializeObject(_estadosRepositorio.GetAll().Select(x => new { Id = x.Id, Descricao = x.Descricao, UF = x.UF }));
        }

        public string GetGrausInstrucao()
        {
            return
                JsonConvert.SerializeObject(
                    _grauInstrucaoRepositorio.GetAll().Select(x => new {Id = x.Id, Descricao = x.Descricao}));
        }

        public void Dispose()
        {
            _repositorio = null;
            _estadosRepositorio = null;
            _grauInstrucaoRepositorio = null;
        }
    }
}
