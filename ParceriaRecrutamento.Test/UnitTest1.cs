﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;

namespace ParceriaRecrutamento.Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void VerificaSeContem100Itens()
        {
            FizzBuzz fizzBuzz = new FizzBuzz();
            var lista = fizzBuzz.GetValues();
            Debug.Assert(lista.Count == 100);
        }
    }
}
