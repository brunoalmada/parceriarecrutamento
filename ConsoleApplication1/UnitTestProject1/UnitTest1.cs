﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ConsoleApplication1;
using System.Diagnostics;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void VerificaSeContem100Itens()
        {
            FizzBuzz fizzBuzz = new FizzBuzz();
            var lista = fizzBuzz.GetValues();
            Assert.AreEqual(100, lista.Count);
        }

        [TestMethod]
        public void VerificaSePrimeiroItemDivisivel()
        {
            FizzBuzz fizzBuzz = new FizzBuzz();
            var lista = fizzBuzz.GetValues();
            var primeiroItem = lista[0];
            Assert.AreEqual("1", primeiroItem);
        }

        [TestMethod]
        public void VerificaSeSegundoItemDivisivel()
        {
            FizzBuzz fizzBuzz = new FizzBuzz();
            var lista = fizzBuzz.GetValues();
            var segundoItem = lista[1];
            Assert.AreEqual("2", segundoItem);
        }

        [TestMethod]
        public void VerificaSeTerceiroItemDivisivel()
        {
            FizzBuzz fizzBuzz = new FizzBuzz();
            var lista = fizzBuzz.GetValues();
            var terceiroItem = lista[2];
            Assert.AreEqual("Fizz", terceiroItem);
        }

        [TestMethod]
        public void VerificaSeQuintoItemDivisivel()
        {
            FizzBuzz fizzBuzz = new FizzBuzz();
            var lista = fizzBuzz.GetValues();
            var item = lista[4];
            Assert.AreEqual("Buzz", item);
        }

        [TestMethod]
        public void VerificaSeSextoItemDivisivel()
        {
            FizzBuzz fizzBuzz = new FizzBuzz();
            var lista = fizzBuzz.GetValues();
            var item = lista[5];
            Assert.AreEqual("Fizz", item);
        }

        [TestMethod]
        public void VerificaSeDezItemDivisivel()
        {
            FizzBuzz fizzBuzz = new FizzBuzz();
            var lista = fizzBuzz.GetValues();
            var item = lista[9];
            Assert.AreEqual("Buzz", item);
        }

        [TestMethod]
        public void VerificaSeQuinzeItemDivisivel()
        {
            FizzBuzz fizzBuzz = new FizzBuzz();
            var lista = fizzBuzz.GetValues();
            var item = lista[14];
            Assert.AreEqual("FizzBuzz", item);
        }
    }
}
