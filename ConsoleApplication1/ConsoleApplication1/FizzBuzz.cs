﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    public class FizzBuzz
    {
        public IList<string> GetValues()
        {
            var retorno = new List<string>(100);
            for (int i = 1; i <= 100; i++)
            {
                var mod3 = i % 3;
                var mod5 = i % 5;

                if (mod3 == 0 && mod5 == 0)
                {
                    retorno.Add("FizzBuzz");
                    continue;
                }

                if (mod3 == 0)
                {
                    retorno.Add("Fizz");
                    continue;
                }
                
                if (mod5 == 0)
                {
                    retorno.Add("Buzz");
                    continue;
                }
                
                retorno.Add(i.ToString());
            }
            return retorno;
        }
    }
}
