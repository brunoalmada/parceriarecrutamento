﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ParceriaRecrutamento.Negocio;

namespace ParceriaRecrutamento.Web.Controllers
{
    public class CandidatosController : Controller
    {
        private static string EstadosJson;
        private static string GrausInstrucaoJson;

        //
        // GET: /Candidatos/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Cadastrar()
        {
            return View("Cadastrar");
        }

        // POST: /Candidadtos/Cadastrar
        [HttpPost]
        public string Cadastrar(string json)
        {
            //$.ajax({ url: "/Candidatos/Cadastrar", type: "POST", data: {json: "{ Nome: 'teste', Endereco: 'Rua A' }" } })
            //$.post("/Candidatos/Cadastrar", { json: "{ Nome: 'Bruno', Endereco: 'Rua abc' }" } )

            var resultado = Newtonsoft.Json.JsonConvert.DeserializeObject<TempModel>(json);

            using (var candidatoNegocio = new CandidatoNegocio())
            {
                candidatoNegocio.Inserir(json);
            }

            return "A";
        }

        [HttpGet]
        public string GetEstados()
        {
            if (!String.IsNullOrEmpty(EstadosJson))
                return EstadosJson;

            using (CandidatoNegocio candidatoNegocio = new CandidatoNegocio())
            {
                EstadosJson = candidatoNegocio.GetEstados();
                return EstadosJson;
            }
        }

        [HttpGet]
        public string GetGrausInstrucao()
        {
            if (!String.IsNullOrEmpty(GrausInstrucaoJson))
                return GrausInstrucaoJson;

            using (CandidatoNegocio candidatoNegocio = new CandidatoNegocio())
            {
                GrausInstrucaoJson = candidatoNegocio.GetGrausInstrucao();
                return GrausInstrucaoJson;
            }
        }

        [HttpGet]
        public string GetEndereco(string Cep)
        {
            return HeitzmannTools.Correios.Cep.PesquisaEndereco(Cep);
        }

        //
        // GET: /Candidatos/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Candidatos/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Candidatos/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Candidatos/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Candidatos/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Candidatos/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Candidatos/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }

    //<cite class="clearfix">—{{review.author}} on {{review.createdOn | date}}</cite> - AngularJS Filter

    public class TempModel
    {
        public string Nome { get; set; }
        public string Endereco { get; set; }
    }
}
