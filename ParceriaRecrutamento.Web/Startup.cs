﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ParceriaRecrutamento.Web.Startup))]
namespace ParceriaRecrutamento.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
