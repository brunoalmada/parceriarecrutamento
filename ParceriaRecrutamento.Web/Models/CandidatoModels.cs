﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ParceriaRecrutamento.Web.Models
{
    public class CandidatoModels
    {
        public string Nome { get; set; }

        public string Endereco { get; set; }

        public string Numero { get; set; }

        public string Complemento { get; set; }

        public string CEP { get; set; }

        public string Bairro { get; set; }
    }
}