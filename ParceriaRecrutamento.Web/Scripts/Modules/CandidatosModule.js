﻿(function () {
    var app = angular.module('Candidatos', []);

    app.controller('CandidatosController', ['$http', function ($http) {
        this.candidato = {};
        var cCont = this;
        cCont.estados = [];
        cCont.GrausInstrucao = [];

        //<select ng-model="review.stars" class="form-control" ng-options="stars for stars in [5,4,3,2,1]"  title="Stars">

        $http.get('/Candidatos/GetEstados')
            .success(function (data) {
                cCont.estados = data;
            });

        $http.get('/Candidatos/GetGrausInstrucao')
            .success(function (data) {
                cCont.GrausInstrucao = data;
            });


        this.adicionarCandidato = function () {

        }
    }
    ]);
})();


$(document).ready(function () {
    $('#txtCep').mask('00000-000');

    $('#txtCep').keyup(function (e) {
        formataCampo(this, '00000-000', e);
    }).blur(function () {
        var esteCampo = this;

        $.ajax({
            type: "GET",
            url: "/Candidatos/GetEndereco",
            data: "Cep=" + $("#txtCep").val(),
            success: function (retorno) {
                var end = JSON.parse(retorno);
                $("#txtEndereco").val(end.Logradouro);
                $("#txtBairro").val(end.Bairro);
                $("#txtCidade").val(end.Cidade);

            }
        });
    });
});

function formataCampo(campo, Mascara, evento) {
    var boleanoMascara;
    var Digitato = evento.keyCode;
    exp = /\-|\.|\/|\(|\)| /g;
    campoSoNumeros = campo.value.toString().replace(exp, "");

    var posicaoCampo = 0;
    var NovoValorCampo = "";
    var TamanhoMascara = campoSoNumeros.length;;

    if (Digitato != 8) {
        for (i = 0; i <= TamanhoMascara; i++) {
            boleanoMascara = ((Mascara.charAt(i) == "-") || (Mascara.charAt(i) == ".")
								|| (Mascara.charAt(i) == "/"))
            boleanoMascara = boleanoMascara || ((Mascara.charAt(i) == "(")
								|| (Mascara.charAt(i) == ")") || (Mascara.charAt(i) == " "))
            if (boleanoMascara) {
                NovoValorCampo += Mascara.charAt(i);
                TamanhoMascara++;
            } else {
                NovoValorCampo += campoSoNumeros.charAt(posicaoCampo);
                posicaoCampo++;
            }
        }
        campo.value = NovoValorCampo;
        return true;
    } else {
        return true;
    }
}

//app.controller('StoreController', ['$http', function ($http) {
//    var store = this;
//    store.products = [];

//    $http.get('/store-products.json')
//    .success(function (data) {
//        store.products = data;
//    });

//}]);